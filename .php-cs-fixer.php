<?php

/*
 * This file is part of the marcoluig/php-cs-fixer-config.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

if (PHP_SAPI !== 'cli') {
    die('This script supports command line usage only. Please check your command.');
}
if (function_exists('xdebug_disable')) {
    xdebug_disable();
}

/*
 * Read composer.json
 */
$string = file_get_contents('composer.json');
$composer = json_decode($string, true);
if (is_array($composer)) {
    $name = $composer['name'];
} else {
    $name = dirname('.');
}

$header = <<<EOF
This file is part of the package $name.

For the full copyright and license information, please read the
LICENSE file that was distributed with this source code.
EOF;

/*
 * Define in which folders to search and which folders to exclude. Exclude some
 * directories that are excluded by Git anyways to speed up the sniffing.
 */
$finder = PhpCsFixer\Finder::create()
    ->exclude(
        [
            'bin',
            'public',
            'var',
            'vendor',
            'web',
        ]
    )
    ->in(getcwd());

$config = new PhpCsFixer\Config();
return $config
    ->setRiskyAllowed(true)
    ->setUsingCache(false)
    ->setRules(
        [
            '@PSR12' => true,
            'array_syntax' => [
                'syntax' => 'short',
            ],
            'binary_operator_spaces' => [
                'operators' => [
                    '=' => 'single_space',
                ],
            ],
            'concat_space' => [
                'spacing' => 'one',
            ],
            'function_typehint_space' => true,
            'general_phpdoc_annotation_remove' => [
                'annotations' => [
                    'author',
                    'package',
                    'subpackage',
                ],
            ],
            'header_comment' => [
                'header' => $header,
            ],
            'native_function_casing' => true,
            'no_alias_functions' => true,
            'no_blank_lines_after_phpdoc' => true,
            'no_empty_statement' => true,
            'no_extra_blank_lines' => true,
            'no_leading_namespace_whitespace' => true,
            'no_short_bool_cast' => true,
            'no_singleline_whitespace_before_semicolons' => true,
            'no_trailing_comma_in_singleline_array' => true,
            'no_unneeded_control_parentheses' => true,
            'no_unused_imports' => true,
            'not_operator_with_successor_space' => true,
            'phpdoc_no_package' => true,
            'phpdoc_order' => true,
            'phpdoc_scalar' => true,
            'phpdoc_separation' => true,
            'single_line_comment_style' => [
                'comment_types' => [
                    'hash',
                ],
            ],
            'single_quote' => true,
            'whitespace_after_comma_in_array' => true,
        ]
    )
    ->setFinder($finder);
