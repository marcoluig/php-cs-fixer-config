# PHP CS Fixer config for TYPO3 CMS Projects 
`.php-cs-fixer.php` helps maintain consistent PHP coding styles for multiple developers working on the same project across various editors and IDEs.
<br><br>

## Install Dependecies
1. PHP package `friendsofphp/php-cs-fixer` is required as dependency. The Package is required in minimum Version `3.4` in the `composer.json` file.  
```
"require": {
    "friendsofphp/php-cs-fixer": "^3.4"
}
```
<br>

## Installation via Composer
If you have a project based on Composer, your can install this package by the following steps:

1. To access this repository via Composer, you have to add it to the repositories array in your projects root `composer.json`
```
"repositories": [
    {
        "type": "git",
        "url": "https://marcoluig@bitbucket.org/marcoluig/editorconfig.git"
    }
]
```
<br>

2. Install this package as dev dependency via:
```
composer req marcoluig/php-cs-fixer-config --dev
```
<br>

3. Add a symlink of the `.php-cs-fixer.php` file from `vendor` to `root` directory in your root `composer.json`.<br>
__Note:__ please change the `vendor` path if it differs in your Composer configuration.
```
"scripts": {
    "post-autoload-dump": [
      "ln -sf vendor/marcoluig/php-cs-fixer-config/.php-cs-fixer.php .php-cs-fixer.php"
    ]
  },
```
<br>

## Manual installation
Download and copy the `.php-cs-fixer.php` to yout project root directory.
<br>
<br>
<br>

# Check and fix PHP coding style
1. To to only check the code and to get feedback of the issues in your command line, start php-cs-fixer in `dry-run` mode. 
```
bin/php-cs-fixer fix ./###YOUR-PATH-TO-CHECK### --config=.php-cs-fixer.php -vv --dry-run
```
<br>

2. To check code and fix the corresponding files automaticly, start php-cs-fixer without params.

```
bin/php-cs-fixer fix ./###YOUR-PATH-TO-CHECK### --config=.php-cs-fixer.php
```
<br><br>

# Change config for other types of projects
To change search settings, edit the following part in the `.php-cs-fixer-config.php` to change the folders to search and exclude.
```
/*
 * Define in which folders to search and which folders to exclude. Exclude some
 * directories that are excluded by Git anyways to speed up the sniffing.
 */
$finder = PhpCsFixer\Finder::create()
    ->exclude(
        [
            'bin',
            'public',
            'var',
            'vendor',
            'web',
        ]
    )
    ->in(getcwd());
```